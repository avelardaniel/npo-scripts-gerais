import csv
import subprocess

with open("filereport_read_run_PRJNA867160_tsv.txt") as file:
	reader = csv.reader(file, delimiter="\t")
	next(reader)

	for line in reader:
		sample_name, links = line[3], line[6].split(";")

		for link in links:
			subprocess.call("wget -c "+link, shell=True)