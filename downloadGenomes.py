import subprocess
import csv
import os

species = [] # Criando a lista de espécies a serem investigadas. A medida que elas forem sendo encontradas elas serão removidas.
with open("sp.txt") as file:
	reader = csv.reader(file)

	for line in reader:
		species.extend(line)

sp2link = {sp: None for sp in species} # Criando o dicionário para associar a espécie ao link para baixar

# Baixando o arquivo de sumário e procurando link para as espécies
subprocess.call("mkdir -p genomes", shell=True)
os.chdir("genomes")
subprocess.call("rm GCF*", shell=True)
subprocess.call("wget -c  https://ftp.ncbi.nlm.nih.gov/genomes/refseq/bacteria/assembly_summary.txt", shell=True)
with open("assembly_summary.txt") as file:
	reader = csv.reader(file, delimiter="\t")
	next(reader)

	for line in reader:
		sp, status, link = line[7], line[11], line[19]

		if sp in species and status == "Complete Genome":
			species.remove(sp)
			sp2link[sp] = link

			if not species:
				print("All species have been found! Terminating.")
				break


print("Espécies que não serão baixadas: ")
print(species)

# Baixando o genoma das espécies
for sp in sp2link:
	link = sp2link[sp]
	
	if link is None:
		continue
	else:
		print(sp)

	gcf = link.split("/")[-1]

	# Download
	subprocess.call("wget -c "+link+"/"+gcf+"_genomic.fna.gz "+link+"/"+gcf+"_genomic.gff.gz", shell=True, stderr=subprocess.DEVNULL)
	subprocess.call("gzip -d *.gz", shell=True)
	subprocess.call("gffread -T "+gcf+"_genomic.gff -o "+gcf+"_genomic.gtf", shell=True)
	subprocess.call("gffread -w "+gcf+".fa -g "+gcf+"_genomic.fna -T "+gcf+"_genomic.gtf", shell=True)

	# Processando o nome do read
	name2symbol = {}

	# Criando e abrindo o arquivo auxiliar para converter as informações do gtf
	subprocess.call("grep 'gene_name' "+gcf+"_genomic.gtf > aux.txt", shell=True)
	with open("aux.txt") as file:
		reader = csv.reader(file, delimiter="\t")

		for line in reader:
			aux = line[-1]
			aux = aux.split(" ")

			geneName, geneSymbol = aux[3][1:-2], aux[5][1:-2]
			name2symbol[geneName] = geneSymbol


	outFile = [] # Arquivo de saída
	header = sp.replace(" ", "_") # Make species in header

	with open(gcf+".fa") as file:
		reader = csv.reader(file, delimiter="\t")

		for line in reader:
			if ">" in line[0]:
				aux = line[0].split(" ")
					
				translate, symbol = aux[0][1:], aux[0][1:] # Obtendo somente o nome do cabeçalho
				translate = ">"+header+"::"+translate

				
				if symbol in name2symbol:
					translate = translate+"::"+name2symbol[symbol]

				aux[0] = translate
				line[0] = " ".join(aux)
			
			outFile.append(line)

	with open(gcf+".fa", "w") as file:
		writer = csv.writer(file, delimiter="\t")

		writer.writerows(outFile)

subprocess.call("cat *.fa > ../genome.fa", shell=True)